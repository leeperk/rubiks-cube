using System.Collections.Generic;
using UnityEngine;

public class Automate : MonoBehaviour
{
    private CubeState _cubeState;
    private ReadCube _readCube;
    private readonly List<string> _allMoves = new()
    {
        "U", "D", "L", "R", "F", "B"
        , "U2", "D2", "L2", "R2", "F2", "B2"
        , "U'", "D'", "L'", "R'", "F'", "B'"
    };
    public static List<string> MoveList = new();

    // Start is called before the first frame update
    void Start()
    {
        _cubeState = FindObjectOfType<CubeState>();
        _readCube = FindObjectOfType<ReadCube>();
    }

    // Update is called once per frame
    void Update()
    {
        if (MoveList.Count > 0 && false == CubeState.IsAutoRotating && CubeState.IsStarted)
        {
            DoMove(MoveList[0]);
            MoveList.RemoveAt(0);
        }
    }

    public void Shuffle()
    {
        var moves = new List<string>();
        var shuffleLength = Random.Range(10, 30);
        for (var i = 0; i < shuffleLength; i++)
        {
            var randomMove = Random.Range(0, _allMoves.Count);
            moves.Add(_allMoves[randomMove]);
        }

        MoveList = moves;
    }

    private void DoMove(string move)
    {
        _readCube.ReadState();
        CubeState.IsAutoRotating = true;
        if (move == "U")
        {
            RotateSide(_cubeState.UpFaces, -90);
        }
        if (move == "U'") {
            RotateSide(_cubeState.UpFaces, 90);
        }
        if (move == "U2")
        {
            RotateSide(_cubeState.UpFaces, -180);
        }
        if (move == "E")
        {
            RotateMiddleSlice(_cubeState.MiddleUpDownFaces, _cubeState.UpFaces[4], 90);
        }
        if (move == "E'")
        {
            RotateMiddleSlice(_cubeState.MiddleUpDownFaces, _cubeState.UpFaces[4], - 90);
        }
        if (move == "E2")
        {
            RotateMiddleSlice(_cubeState.MiddleUpDownFaces, _cubeState.UpFaces[4], 180);
        }
        if (move == "D")
        {
            RotateSide(_cubeState.DownFaces, -90);
        }
        if (move == "D'")
        {
            RotateSide(_cubeState.DownFaces, 90);
        }
        if (move == "D2")
        {
            RotateSide(_cubeState.DownFaces, -180);
        }

        if (move == "L")
        {
            RotateSide(_cubeState.LeftFaces, -90);
        }
        if (move == "L'")
        {
            RotateSide(_cubeState.LeftFaces, 90);
        }
        if (move == "L2")
        {
            RotateSide(_cubeState.LeftFaces, -180);
        }
        if (move == "M")
        {
            RotateMiddleSlice(_cubeState.MiddleLeftRightFaces, _cubeState.LeftFaces[4], 90);
        }
        if (move == "M'")
        {
            RotateMiddleSlice(_cubeState.MiddleLeftRightFaces, _cubeState.LeftFaces[4], -90);
        }
        if (move == "M2")
        {
            RotateMiddleSlice(_cubeState.MiddleLeftRightFaces, _cubeState.LeftFaces[4], 180);
        }
        if (move == "R")
        {
            RotateSide(_cubeState.RightFaces, -90);
        }
        if (move == "R'")
        {
            RotateSide(_cubeState.RightFaces, 90);
        }
        if (move == "R2")
        {
            RotateSide(_cubeState.RightFaces, -180);
        }

        if (move == "F")
        {
            RotateSide(_cubeState.FrontFaces, -90);
        }
        if (move == "F'")
        {
            RotateSide(_cubeState.FrontFaces, 90);
        }
        if (move == "F2")
        {
            RotateSide(_cubeState.FrontFaces, -180);
        }
        if (move == "S")
        {
            RotateMiddleSlice(_cubeState.MiddleFrontBackFaces, _cubeState.FrontFaces[4], 90);
        }
        if (move == "S'")
        {
            RotateMiddleSlice(_cubeState.MiddleFrontBackFaces, _cubeState.FrontFaces[4], -90);
        }
        if (move == "S2")
        {
            RotateMiddleSlice(_cubeState.MiddleFrontBackFaces, _cubeState.FrontFaces[4], 180);
        }
        if (move == "B")
        {
            RotateSide(_cubeState.BackFaces, -90);
        }
        if (move == "B'")
        {
            RotateSide(_cubeState.BackFaces, 90);
        }
        if (move == "B2")
        {
            RotateSide(_cubeState.BackFaces, -180);
        }
    }

    private void RotateSide(List<GameObject> sides, float angle)
    {
        var axis = Vector3.zero - sides[4].transform.parent.localPosition;
        var pivotRotation = sides[4].transform.parent.GetComponent<PivotRotation>();
        pivotRotation.StartAutoRotate(sides, axis, angle);

    }

    private void RotateMiddleSlice(List<GameObject> sides, GameObject faceForAxis, float angle)
    {
        var axis = Vector3.zero - faceForAxis.transform.parent.localPosition;
        var pivotRotation = _cubeState.CenterCube.GetComponent<PivotRotation>();
        pivotRotation.StartAutoRotate(sides, axis, angle);

    }
}
