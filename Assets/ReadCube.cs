using System.Collections.Generic;
using UnityEngine;

public class ReadCube : MonoBehaviour
{
    public Transform UpRay;
    public Transform DownRay;
    public Transform LeftRay;
    public Transform RightRay;
    public Transform FrontRay;
    public Transform BackRay;
    public GameObject emptyGO;

    private int _layerMask = 1 << 3;
    private CubeState _cubeState;
    private CubeMap _cubeMap;
    private List<GameObject> _frontRays = new();
    private List<GameObject> _middleFrontBackRays = new();
    private List<GameObject> _backRays = new();
    
    private List<GameObject> _upRays = new();
    private List<GameObject> _middleUpDownRays = new();
    private List<GameObject> _downRays = new();
    
    private List<GameObject> _leftRays = new();
    private List<GameObject> _middleLeftRightRays = new();
    private List<GameObject> _rightRays = new();

    // Start is called before the first frame update
    private void Start()
    {
        SetRayTransforms();

        _cubeState = FindObjectOfType<CubeState>();
        _cubeMap = FindObjectOfType<CubeMap>();
        ReadState();
        CubeState.IsStarted = true;

        var facesHit = new List<GameObject>();
        var ray = FrontRay.transform.position;

        if (Physics.Raycast(ray, FrontRay.right, out var hit, Mathf.Infinity, _layerMask))
        {
            Debug.DrawRay(ray, FrontRay.right * hit.distance, Color.yellow);
            //facesHit.Add(hit.collider.gameObject);
        }
        else
        {
            Debug.DrawRay(ray, FrontRay.right * 1000, Color.green);
        }
        _cubeState.FrontFaces = facesHit;
        _cubeMap.Set();
    }

    // Update is called once per frame
    private void Update()
    {

    }

    public void ReadState()
    {
        _cubeState = FindObjectOfType<CubeState>();
        _cubeMap = FindObjectOfType<CubeMap>();

        _cubeState.UpFaces = ReadFace(_upRays);
        _cubeState.MiddleUpDownFaces = ReadMiddleUpDownFace();
        _cubeState.DownFaces = ReadFace(_downRays);

        _cubeState.LeftFaces = ReadFace(_leftRays);
        _cubeState.MiddleLeftRightFaces = ReadMiddleLeftRightFace();
        _cubeState.RightFaces = ReadFace(_rightRays);

        _cubeState.FrontFaces = ReadFace(_frontRays);
        _cubeState.MiddleFrontBackFaces = ReadMiddleFrontBackFace();
        _cubeState.BackFaces = ReadFace(_backRays);

        _cubeMap.Set();
    }

    private void SetRayTransforms()
    {
        _upRays = BuildRays(UpRay, new Vector3(90, 90, 0));
        _downRays = BuildRays(DownRay, new Vector3(270, 90, 0));

        _leftRays = BuildRays(LeftRay, new Vector3(0, 180, 0));
        _rightRays = BuildRays(RightRay, new Vector3(0, 0, 0));

        _frontRays = BuildRays(FrontRay, new Vector3(0, 90, 0));
        _backRays = BuildRays(BackRay, new Vector3(0, 270, 0));

        _middleUpDownRays = BuildMiddleUpDownRays();
        _middleLeftRightRays = BuildMiddleLeftRightRays();
        _middleFrontBackRays = BuildMiddleFrontBackRays();
    }

    private List<GameObject> BuildRays(Transform rayTransform, Vector3 direction)
    {
        var rayCount = 0;
        var rays = new List<GameObject>();
        for (var y = 1; y > -2; y--)
        {
            for (var x = -1; x < 2; x++)
            {
                var startPosition = new Vector3(
                    rayTransform.localPosition.x + x
                    , rayTransform.localPosition.y + y
                    , rayTransform.localPosition.z
                );
                var rayStart = Instantiate(emptyGO, startPosition, Quaternion.identity, rayTransform);
                rayStart.name = rayCount.ToString();
                rays.Add(rayStart);
                rayCount++;
            }
        }

        rayTransform.localRotation = Quaternion.Euler(direction);
        return rays;
    }

    private List<GameObject> BuildMiddleUpDownRays()
    {
        var rays = new List<GameObject>()
        {
            _frontRays[3], _frontRays[4], _frontRays[5], _rightRays[4], _backRays[3],  _backRays[4],  _backRays[5], _leftRays[4]
        };

        return rays;
    }

    private List<GameObject> BuildMiddleLeftRightRays()
    {
        var rays = new List<GameObject>()
        {
            _upRays[1], _upRays[4], _upRays[7], _frontRays[4], _downRays[1],  _downRays[4],  _downRays[7], _backRays[4]
        };

        return rays;
    }

    private List<GameObject> BuildMiddleFrontBackRays()
    {
        var rays = new List<GameObject>()
        {
            _upRays[3], _upRays[4], _upRays[5], _rightRays[4], _downRays[5],  _downRays[4],  _downRays[3], _leftRays[4]
        };

        return rays;
    }

    public List<GameObject> ReadFace(List<GameObject> rayStarts)
    {
        var facesHit = new List<GameObject>();

        foreach (var rayStart in rayStarts)
        {
            var ray = rayStart.transform.position;

            if (Physics.Raycast(ray, rayStart.transform.forward, out var hit, Mathf.Infinity, _layerMask))
            {
                Debug.DrawRay(ray, rayStart.transform.forward * hit.distance, Color.green, 1);
                facesHit.Add(hit.collider.gameObject);
            }
            else
            {
                Debug.DrawRay(ray, rayStart.transform.forward * 1000, Color.red, 0);
            }
        }
        return facesHit;
    }

    public List<GameObject> ReadMiddleUpDownFace()
    {
        var facesHit = new List<GameObject>();
        foreach (var rayStart in _middleUpDownRays)
        {
            var ray = rayStart.transform.position;
            if (Physics.Raycast(ray, rayStart.transform.forward, out var hit, Mathf.Infinity, _layerMask))
            {
                facesHit.Add(hit.collider.gameObject);
            }
        }
        return facesHit;
    }

    public List<GameObject> ReadMiddleLeftRightFace()
    {
        var facesHit = new List<GameObject>();
        foreach (var rayStart in _middleLeftRightRays)
        {
            var ray = rayStart.transform.position;
            if (Physics.Raycast(ray, rayStart.transform.forward, out var hit, Mathf.Infinity, _layerMask))
            {
                facesHit.Add(hit.collider.gameObject);
            }
        }
        return facesHit;
    }

    public List<GameObject> ReadMiddleFrontBackFace()
    {
        var facesHit = new List<GameObject>();
        foreach (var rayStart in _middleFrontBackRays)
        {
            var ray = rayStart.transform.position;
            if (Physics.Raycast(ray, rayStart.transform.forward, out var hit, Mathf.Infinity, _layerMask))
            {
                facesHit.Add(hit.collider.gameObject);
            }
        }
        return facesHit;
    }
}
