using System.Collections.Generic;
using UnityEngine;

public class PivotRotation : MonoBehaviour
{
    private List<GameObject> _activeSideFaces;
    private Vector3 _localForward;
    private Vector3 _mouseRef;
    private bool _isDragging = false;
    private ReadCube _readCube;
    private CubeState _cubeState;
    private float _sensitivity = 0.4f;
    private Vector3 _rotation;
    private bool _isAutoRotating = false;
    private Quaternion _targetQuaternion;
    private float _speed = 300f;
    private int _rotationFrames;

    // Start is called before the first frame update
    void Start()
    {
        _readCube = FindObjectOfType<ReadCube>();
        _cubeState = FindObjectOfType<CubeState>();
    }

    // LateUpdate is called once per frame at the end
    void LateUpdate()
    {
        if (_isDragging && false == _isAutoRotating)
        {
            SpinSide(_activeSideFaces);
            if (Input.GetKeyUp(KeyCode.Space))
            {
                _isDragging = false;
                RotateToRightAngle();
            }
        }

        if (_isAutoRotating)
        {
            AutoRotate();
        }
    }
    
    private void SpinSide(List<GameObject> side)
    {
        _rotation = Vector3.zero;
        var mouseOffset = Input.mousePosition - _mouseRef;
        if (side == _cubeState.UpFaces)
        {
            _rotation.y = (mouseOffset.x + mouseOffset.y) * _sensitivity * 1;
        }
        if (side == _cubeState.DownFaces)
        {
            _rotation.y = (mouseOffset.x + mouseOffset.y) * _sensitivity * -1;
        }
        if (side == _cubeState.LeftFaces)
        {
            _rotation.z = (mouseOffset.x + mouseOffset.y) * _sensitivity * 1;
        }
        if (side == _cubeState.RightFaces)
        {
            _rotation.z = (mouseOffset.x + mouseOffset.y) * _sensitivity * -1;
        }
        if (side == _cubeState.FrontFaces)
        {
            _rotation.x = (mouseOffset.x + mouseOffset.y) * _sensitivity * -1;
        }
        if (side == _cubeState.BackFaces)
        {
            _rotation.x = (mouseOffset.x + mouseOffset.y) * _sensitivity * 1;
        }
        transform.Rotate(_rotation, Space.Self);
        _mouseRef = Input.mousePosition;
    }

    public void Rotate(List<GameObject> side)
    {
        _activeSideFaces = side;
        _mouseRef = Input.mousePosition;
        _isDragging = true;

        _localForward = Vector3.zero - side[4].transform.parent.transform.localPosition;
    }

    public void StartAutoRotate(List<GameObject> sideFaces, Vector3 axis, float angle)
    {
        _cubeState.PickUp(sideFaces);
        _targetQuaternion = Quaternion.AngleAxis(angle, axis) * transform.localRotation;
        _activeSideFaces = sideFaces;
        _isAutoRotating = true;
        _rotationFrames = 0;
    }

    public void RotateToRightAngle()
    {
        var vector = transform.localEulerAngles;
        vector.x = Mathf.Round(vector.x / 90) * 90;
        vector.y = Mathf.Round(vector.y / 90) * 90;
        vector.z = Mathf.Round(vector.z / 90) * 90;
        _targetQuaternion.eulerAngles = vector;
        _isAutoRotating = true;
    }

    private void AutoRotate()
    {
        _rotationFrames++;
        _isDragging = false;
        var step = _speed * Time.deltaTime;
        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, _targetQuaternion, step);
        if (Quaternion.Angle(transform.localRotation, _targetQuaternion) <= 1)
        {
            _rotationFrames = 0;
            transform.localRotation = _targetQuaternion;
            _cubeState.PutDown(_activeSideFaces, transform.parent);
            _readCube.ReadState();
            CubeState.IsAutoRotating = false;
            _isAutoRotating = false;
            _isDragging = false;
        }
    }
}
