using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;

public class RotateWholeCube : MonoBehaviour
{
    private Vector2 _mouseButtonDownPosition;
    private Vector2 _mouseButtonUpPosition;
    private Vector2 _currentSwipeVector2;
    private float _speed = 200f;
    //private Vector3 _previousMousePosition;
    //private Vector3 _mouseDelta;

    public GameObject TargetCubeRotation;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        ProcessWholeCubeSwipeBeginOrEnd();
        ProcessWholeCubeRotation();
    }

    void ProcessWholeCubeRotation()
    {
        if (transform.rotation != TargetCubeRotation.transform.rotation)
        {
            var step = _speed * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, TargetCubeRotation.transform.rotation, step);
        }
    }

    void ProcessWholeCubeSwipeBeginOrEnd()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _mouseButtonDownPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(1))
        {
            _mouseButtonUpPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _currentSwipeVector2 = new Vector2(_mouseButtonUpPosition.x - _mouseButtonDownPosition.x, _mouseButtonUpPosition.y - _mouseButtonDownPosition.y);
            _currentSwipeVector2.Normalize();
            ProcessWholeCubeSwipe();
        }
    }

    private void ProcessWholeCubeSwipe()
    {
        if (IsLeftSwipe())
        {
            TargetCubeRotation.transform.Rotate(0, 90, 0, Space.World);
        }
        else if (IsRightSwipe())
        {
            TargetCubeRotation.transform.Rotate(0, -90, 0, Space.World);
        }
        else if (IsUpLeftSwipe())
        {
            TargetCubeRotation.transform.Rotate(90, 0, 0, Space.World);
        }
        else if (IsDownLeftSwipe())
        {
            TargetCubeRotation.transform.Rotate(0, 0, 90, Space.World);
        }
        else if (IsUpRightSwipe())
        {
            TargetCubeRotation.transform.Rotate(0, 0, -90, Space.World);
        }
        else if (IsDownRightSwipe())
        {
            TargetCubeRotation.transform.Rotate(-90, 0, 0, Space.World);
        }
    }

    bool IsLeftSwipe()
    {
        return _currentSwipeVector2.x < 0 && _currentSwipeVector2.y is > -0.5f and < 0.5f;
    }

    bool IsRightSwipe()
    {
        return _currentSwipeVector2.x > 0 && _currentSwipeVector2.y is > -0.5f and < 0.5f;
    }

    bool IsUpLeftSwipe()
    {
        return _currentSwipeVector2.y > 0 && _currentSwipeVector2.x < 0;
    }

    bool IsUpRightSwipe()
    {
        return _currentSwipeVector2.y > 0 && _currentSwipeVector2.x >= 0;
    }

    bool IsDownLeftSwipe()
    {
        return _currentSwipeVector2.y < 0 && _currentSwipeVector2.x < 0;
    }

    bool IsDownRightSwipe()
    {
        return _currentSwipeVector2.y < 0 && _currentSwipeVector2.x >= 0;
    }
}
