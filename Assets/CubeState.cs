using System.Collections.Generic;
using UnityEngine;

public class CubeState : MonoBehaviour
{
    public List<GameObject> FrontFaces = new();
    public List<GameObject> MiddleFrontBackFaces = new();
    public List<GameObject> BackFaces = new();

    public List<GameObject> UpFaces = new();
    public List<GameObject> MiddleUpDownFaces = new();
    public List<GameObject> DownFaces = new();
    
    public List<GameObject> LeftFaces = new();
    public List<GameObject> MiddleLeftRightFaces = new();
    public List<GameObject> RightFaces = new();
    
    public GameObject CenterCube;

    public static bool IsAutoRotating = false;
    public static bool IsStarted = false;

    private readonly IReadOnlyList<string> _frontNames = new List<string>()
    {
        "FLU", "FU", "FRU",
        "FL",  "F",  "FR",
        "FLD", "FD", "FRD"
    };
    private readonly IReadOnlyList<string> _backNames = new List<string>()
    {
        "RBU", "BU", "LBR",
        "RB",  "B",  "LB",
        "RBD", "BD", "LBD"
    };
    private readonly IReadOnlyList<string> _upNames = new List<string>()
    {
        "LBU", "BU", "RBU",
        "LU",  "U",  "RU",
        "FLU", "FU", "FRU"
    };
    private readonly IReadOnlyList<string> _downNames = new List<string>()
    {
        "FLD", "FD", "FRD",
        "LD",  "D",  "RD",
        "LBD", "BD", "RBD"
    };
    private readonly IReadOnlyList<string> _leftNames = new List<string>()
    {
        "LBU", "LU", "FLU",
        "LB",  "L",  "FL",
        "LBD", "LD", "FLD"
    };
    private readonly IReadOnlyList<string> _rightNames = new List<string>()
    {
        "FRU", "RU", "RBU",
        "FR",  "R",  "RB",
        "FRD", "RD", "RBD"
    };

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.B))
        {
            Debug.Break();
        }
    }

    private string GetSideString(List<GameObject> side)
    {
        var sideString = "";
        foreach (var face in side)
        {
            sideString += face.name[0];
        }
        return sideString;
    }

    private int FindIndexOfFace(GameObject face, List<GameObject> faces)
    {
        return faces.IndexOf(face);
    }

    public void PickUp(List<GameObject> cubeSideFaces)
    {
        var isMiddleSlice = cubeSideFaces.Count == 8;
        var transformOfParentCube = cubeSideFaces.Count == 9 ? cubeSideFaces[4].transform.parent : transform.gameObject.GetComponent<CubeState>().CenterCube.transform;
        foreach (GameObject face in cubeSideFaces)
        {
            if (isMiddleSlice || face != cubeSideFaces[4])
            {
                face.transform.parent.transform.parent = transformOfParentCube;
            }
        }
    }

    public void PutDown(List<GameObject> cubeSideFaces, Transform newParent)
    {
        var isMiddleSlice = cubeSideFaces.Count == 8;
        foreach (var cubeFace in cubeSideFaces)
        {
            if (isMiddleSlice || cubeFace != cubeSideFaces[4])
            {
                cubeFace.transform.parent.transform.parent = newParent;
            }
        }
    }

    public string GetStateString()
    {
        var stateString = "";
        stateString += GetSideString(UpFaces);
        stateString += GetSideString(RightFaces);
        stateString += GetSideString(FrontFaces);
        stateString += GetSideString(DownFaces);
        stateString += GetSideString(LeftFaces);
        stateString += GetSideString(BackFaces);

        return stateString;
    }

    public (string, char) GetCubePositionAndFaceNames(GameObject face)
    {
        var index = FindIndexOfFace(face, UpFaces);
        if (index >= 0)
        {
            return (_upNames[index], 'U');
        }
        index = FindIndexOfFace(face, DownFaces);
        if (index >= 0)
        {
            return (_downNames[index], 'D');
        }
        index = FindIndexOfFace(face, FrontFaces);
        if (index >= 0)
        {
            return (_frontNames[index], 'F');
        }
        index = FindIndexOfFace(face, BackFaces);
        if (index >= 0)
        {
            return (_backNames[index], 'B');
        }
        index = FindIndexOfFace(face, LeftFaces);
        if (index >= 0)
        {
            return (_leftNames[index], 'L');
        }
        index = FindIndexOfFace(face, RightFaces);
        if (index >= 0)
        {
            return (_rightNames[index], 'R');
        }
        return ("", '\0');
    }
}
