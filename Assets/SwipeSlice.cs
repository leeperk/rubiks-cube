using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SwipeSlice : MonoBehaviour
{
    private CubeState _cubeState;
    private ReadCube _readCube;
    private int _layerMask = 1 << 3;
    private string _startingSwipeCubePosition = "";
    private char _startingSwipeFaceName = '\0';
    private string _endingSwipeCubePosition = "";
    private char _endingSwipeFaceName = '\0';

    private readonly IReadOnlyList<(string cubPosition, char faceName)> _upSlice = new List<(string cubPosition, char faceName)>()
    {
        ("FLU", 'F')
        , ("FU", 'F')
        , ("FRU", 'F')
        , ("FRU", 'R')
        , ("RU", 'R')
        , ("RBU", 'R')
        , ("RBU", 'B')
        , ("BU", 'B')
        , ("LBU", 'B')
        , ("LBU", 'L')
        , ("LU", 'L')
        , ("FLU", 'L')
    };
    private readonly IReadOnlyList<(string cubPosition, char faceName)> _middleUpDownSlice = new List<(string cubPosition, char faceName)>()
    {
        ("FL", 'F')
        , ("F", 'F')
        , ("FR", 'F')
        , ("FR", 'R')
        , ("R", 'R')
        , ("RB", 'R')
        , ("RB", 'B')
        , ("B", 'B')
        , ("LB", 'B')
        , ("LB", 'L')
        , ("L", 'L')
        , ("FL", 'L')
    };
    private readonly IReadOnlyList<(string cubPosition, char faceName)> _downSlice = new List<(string cubPosition, char faceName)>()
    {
        ("FLD", 'F')
        , ("FD", 'F')
        , ("FRD", 'F')
        , ("FRD", 'R')
        , ("RD", 'R')
        , ("RBD", 'R')
        , ("RBD", 'B')
        , ("BD", 'B')
        , ("LBD", 'B')
        , ("LBD", 'L')
        , ("LD", 'L')
        , ("FLD", 'L')
    };

    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _leftSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "LBU", 'U' )
        , ( "LU", 'U' )
        , ( "FLU", 'U' )
        , ( "FLU", 'F' )
        , ( "FL", 'F' )
        , ( "FLD", 'F' )
        , ( "FLD", 'D' )
        , ( "LD", 'D' )
        , ( "LBD", 'D' )
        , ( "LBD", 'B' )
        , ( "LB", 'B' )
        , ( "LBU", 'B' )

    };
    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _middleLeftRightSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "BU", 'U' )
        , ( "U", 'U' )
        , ( "FU", 'U' )
        , ( "FU", 'F' )
        , ( "F", 'F' )
        , ( "FD", 'F' )
        , ( "FD", 'D' )
        , ( "D", 'D' )
        , ( "BD", 'D' )
        , ( "BD", 'B' )
        , ( "B", 'B' )
        , ( "BU", 'B' )
    };
    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _rightSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "RBU", 'U' )
        , ( "RU", 'U' )
        , ( "FRU", 'U' )
        , ( "FRU", 'F' )
        , ( "FR", 'F' )
        , ( "FRD", 'F' )
        , ( "FRD", 'D' )
        , ( "RD", 'D' )
        , ( "RBD", 'D' )
        , ( "RBD", 'B' )
        , ( "RB", 'B' )
        , ( "RBU", 'B' )

    };

    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _frontSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "FLU", 'U' )
        , ( "FU", 'U' )
        , ( "FRU", 'U' )
        , ( "FRU", 'R' )
        , ( "FR", 'R' )
        , ( "FRD", 'R' )
        , ( "FRD", 'D' )
        , ( "FD", 'D' )
        , ( "FLD", 'D' )
        , ( "FLD", 'L' )
        , ( "FL", 'L' )
        , ( "FLU", 'L' )
    };
    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _middleFrontBackSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "LU", 'U' )
        , ( "U", 'U' )
        , ( "RU", 'U' )
        , ( "RU", 'R' )
        , ( "R", 'R' )
        , ( "RD", 'R' )
        , ( "RD", 'D' )
        , ( "D", 'D' )
        , ( "LD", 'D' )
        , ( "LD", 'L' )
        , ( "L", 'L' )
        , ( "LU", 'L' )
    };
    private readonly IReadOnlyList<(string cubePosition, char faceNext)> _backSlice = new List<(string cubePosition, char faceNext)>()
    {
        ( "LBU", 'U' )
        , ( "BU", 'U' )
        , ( "RBU", 'U' )
        , ( "RBU", 'R' )
        , ( "RB", 'R' )
        , ( "RBD", 'R' )
        , ( "RBD", 'D' )
        , ( "BD", 'D' )
        , ( "LBD", 'D' )
        , ( "LBD", 'L' )
        , ( "LB", 'L' )
        , ( "LBU", 'L' )
    };

    // Start is called before the first frame update
    void Start()
    {
        _cubeState = FindObjectOfType<CubeState>();
        _readCube = FindObjectOfType<ReadCube>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CubeState.IsAutoRotating)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            _readCube.ReadState();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 100.0f, _layerMask))
            {
                (_startingSwipeCubePosition, _startingSwipeFaceName) = _cubeState.GetCubePositionAndFaceNames(hit.collider.gameObject);
            }
            else
            {
                ClearSwipe();
            }
        }

        if (Input.GetMouseButtonUp(0) && _startingSwipeCubePosition.Length > 0)
        {
            _readCube.ReadState();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 100.0f, _layerMask))
            {
                (_endingSwipeCubePosition, _endingSwipeFaceName) = _cubeState.GetCubePositionAndFaceNames(hit.collider.gameObject);
                //print($"{_startingSwipeCubePosition}/{_startingSwipeFaceName} -> {_endingSwipeCubePosition}/{_endingSwipeFaceName}");
                RotateSlice();
            }
            else
            {
                ClearSwipe();
            }
        }
    }

    private void ClearSwipe()
    {
        _startingSwipeCubePosition = "";
        _startingSwipeFaceName = '\0';
        _endingSwipeCubePosition = "";
        _endingSwipeFaceName = '\0';
    }

    private void RotateSlice()
    {
        if (_startingSwipeCubePosition == _endingSwipeCubePosition && _startingSwipeFaceName == _endingSwipeFaceName)
        {
            return;
        }

        var (startIndex, endIndex) = GetSwipeIndexesInSlice(_upSlice);
        var move = "";
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"U{(startIndex < endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_middleUpDownSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"E{(startIndex > endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_downSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"D{(startIndex > endIndex ? "'" : "")}";
        }

        (startIndex, endIndex) = GetSwipeIndexesInSlice(_leftSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"L{(startIndex > endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_middleLeftRightSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"M{(startIndex < endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_rightSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"R{(startIndex < endIndex ? "'" : "")}";
        }

        (startIndex, endIndex) = GetSwipeIndexesInSlice(_frontSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"F{(startIndex > endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_middleFrontBackSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"S{(startIndex < endIndex ? "'" : "")}";
        }
        (startIndex, endIndex) = GetSwipeIndexesInSlice(_backSlice);
        if (startIndex >= 0 && endIndex >= 0)
        {
            move = $"B{(startIndex < endIndex ? "'" : "")}";
        }

        if (move.Length > 0)
        {
            print($"Automating move {move}");
            Automate.MoveList = new List<string> { move };
        }
    }

    private (int startIndex, int endIndex) GetSwipeIndexesInSlice(IReadOnlyList<(string cubePosition, char faceName)> cubeAndFaceNames)
    {
        int GetIndexInSliceNames(string cubePosition, char faceName) =>
            cubeAndFaceNames.Select((cfn, i) => new { cfn, i = i + 1 })
                .Where(cfni => cfni.cfn.cubePosition == cubePosition && cfni.cfn.faceName == faceName)
                .Select(cfni => cfni.i)
                .FirstOrDefault() - 1;

        return (
            GetIndexInSliceNames(_startingSwipeCubePosition, _startingSwipeFaceName)
            , GetIndexInSliceNames(_endingSwipeCubePosition, _endingSwipeFaceName)
        );
    }

    private IEnumerable<char> GetCommonSideLetters()
    {
        return _startingSwipeCubePosition.Intersect(_endingSwipeCubePosition);
    }

    private char GetEdgeSliceName()
    {
        var commonSideLetters = GetCommonSideLetters();
        return commonSideLetters.FirstOrDefault(letter => letter != _startingSwipeFaceName && letter != _endingSwipeFaceName);
    }

    private char GetCenterSliceName()
    {
        return '\0';
    }
}
