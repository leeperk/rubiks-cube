using System.Collections.Generic;
using UnityEngine;

public class SelectFace : MonoBehaviour
{
    private CubeState _cubeState;
    private ReadCube _readCube;
    private int _layerMask = 1 << 3;

    // Start is called before the first frame update
    void Start()
    {
        _cubeState = FindObjectOfType<CubeState>();
        _readCube= FindObjectOfType<ReadCube>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && false == CubeState.IsAutoRotating)
        {
            _readCube.ReadState();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, 100.0f, _layerMask))
            {
                var face = hit.collider.gameObject;
                var cubeSides = new List<List<GameObject>>()
                {
                    _cubeState.UpFaces
                    , _cubeState.DownFaces
                    , _cubeState.LeftFaces
                    , _cubeState.RightFaces
                    , _cubeState.FrontFaces
                    , _cubeState.BackFaces
                };
                foreach (var cubeSide in cubeSides)
                {
                    if (cubeSide.Contains(face))
                    {
                        _cubeState.PickUp(cubeSide);
                        cubeSide[4].transform.parent.GetComponent<PivotRotation>().Rotate(cubeSide);
                    }
                }

            }
        }
    }
}
