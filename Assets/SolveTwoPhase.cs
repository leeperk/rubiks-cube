using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Kociemba;

public class SolveTwoPhase : MonoBehaviour
{
    public ReadCube _readCube;
    public CubeState _cubeState;

    private bool _needsPreSolve = true;

    // Start is called before the first frame update
    void Start()
    {
        _readCube = FindObjectOfType<ReadCube>();
        _cubeState = FindObjectOfType<CubeState>();
    }

    // Update is called once per frame
    void Update()
    {
        if (CubeState.IsStarted && _needsPreSolve)
        {
            _needsPreSolve = false;
            Solver();
        }
    }

    public void Solver()
    {
        _readCube.ReadState();
        var moveString = _cubeState.GetStateString();
        print($"Current State: {moveString}");

        //var solution = SearchRunTime.solution(moveString, out var info, buildTables: true);
        var solution = Search.solution(moveString, out var info);
        print($"Solution: {solution}");
        print($"Info: {info}");
        if (solution.StartsWith("Error"))
        {
            return;
        }

        var solutionList = StringToList(solution);
        Automate.MoveList = solutionList;

    }

    private List<string> StringToList(string solution)
    {
        return solution.Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();
    }
}
