using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubeMap : MonoBehaviour
{
    private CubeState _cubeState;

    public Transform UpMap;
    public Transform DownMap;
    public Transform LeftMap;
    public Transform RightMap;
    public Transform FrontMap;
    public Transform BackMap;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Set()
    {
        _cubeState = FindObjectOfType<CubeState>();
        UpdateMap(_cubeState.FrontFaces, FrontMap);
        UpdateMap(_cubeState.BackFaces, BackMap);
        UpdateMap(_cubeState.LeftFaces, LeftMap);
        UpdateMap(_cubeState.RightFaces, RightMap);
        UpdateMap(_cubeState.UpFaces, UpMap);
        UpdateMap(_cubeState.DownFaces, DownMap);

    }

    void UpdateMap(List<GameObject> face, Transform side)
    {
        int i = 0;
        foreach (Transform map in side)
        {
            if (face[i].name[0] == 'F')
            {
                map.GetComponent<Image>().color = new Color(1, 0.5f, 0, 1);
            }
            if (face[i].name[0] == 'B')
            {
                map.GetComponent<Image>().color = Color.red;
            }
            if (face[i].name[0] == 'U')
            {
                map.GetComponent<Image>().color = Color.yellow;
            }
            if (face[i].name[0] == 'D')
            {
                map.GetComponent<Image>().color = Color.white;
            }
            if (face[i].name[0] == 'L')
            {
                map.GetComponent<Image>().color = Color.green;
            }
            if (face[i].name[0] == 'R')
            {
                map.GetComponent<Image>().color = Color.blue;
            }
            i++;
        }
    }
}
